var express = require('express');
var sanitizer = require('sanitizer');
var app = express();
config = require(__dirname+'/config/config.json');

//var profiler = require('v8-profiler-node8');

// Load sources
require('./src/entity');
require('./client/inventory');
require('./src/database');

/* <!-- SECURE SOCKET LAYER (SSL) PREPARE -->
Here we just prepare the things for the client such as files and html,
the most important thing is HTTPS*/
const https = require("https"),
    fs = require("fs");

const options = {
    key: fs.readFileSync("./config/keys/key.pem"),
    cert: fs.readFileSync("./config/keys/cert.pem"),
    passphrase: "P@ssw0rd"
};

// Send html to the client when attacking our ip
 app.get('/',function(req,res) {
     res.sendFile(__dirname+'/client/client.html');
     app.use(express.static(__dirname+'/client'));
});
// Send whole client folder as resource (so we can load imgs or other resources)

var server = https.createServer(options, app);
// <!-- END OF SECURE SOCKET LAYER PREPARE -->

// Specify in which port the server listens to
server.listen(config.server.port);
console.log('Server listening on: ' + config.server.port);

// List of sockets (connection)
SOCKET_LIST = {}

try {
    // Connect
    var io = require('socket.io')(server,{});
    io.sockets.on('connection', socket => {
        // Make this truly unique and random and not a Math.random()
        socket.id = Math.random();
        SOCKET_LIST[socket.id] = socket;

        // User sign
        socket.on('signIn',data => {
            Database.isValidPassword(data,function(res) {
                if(!res) return socket.emit('signInResponse',{success:false});
                Database.getPlayerProgress(data.username,function(progress) {
                    Player.onConnect(socket,data.username,progress);
                    socket.emit('signInResponse',{success:true});
                })
            });
        });

        // User register
        socket.on('signUp',data => {
            Database.isUsernameTaken(data,function(res) {
                if(res) {
                    socket.emit('signUpResponse',{success:false});
                }
                else {
                    Database.addUser(data, function() {
                        socket.emit('signUpResponse',{success:true});
                    });
                }
            });
        });

        // Evaluate variables for debug purposes
        socket.on('evalServer',msg => {
            if (!config.debug) return; 
            try {
                var result = eval(msg);
            } catch(e) {
                result = e.message;
            }
            socket.emit('evalAnswer',result);
        });


        // Disconnect
        socket.on('disconnect',() => {
            delete SOCKET_LIST[socket.id];
            Player.onDisconnect(socket);
        });


    });


    setInterval(() => {
        var packets = Entity.getFrameUpdateData();
        // Send packets
        io.sockets.emit('initialize',packets.initPacket);
        io.sockets.emit('update',packets.updatePacket);
        io.sockets.emit('remove',packets.removePacket);
        

    },config.server.tickRate);

    // Profiling desactivated due to dockerization problems (has to do with the library)

    /*var startProfiling = duration => {
        profiler.startProfiling('1', true);
        setTimeout(() => {
            var profile1 = profiler.stopProfiling('1');

            profile1.export(function(error, result) {
                fs.writeFile('./logs/profile.cpuprofile', result, err => {
                    if (err) throw err;
                    console.log('Saved CPU profile.');
                });
                profile1.delete();
            });
        },duration);
    }

    startProfiling(10000);*/

}catch(e){
    fs.appendFile('./logs/error.log', '['+ (new Date()).toUTCString() + ']: ' + e.message + '\n', function(err) {
        if (err) throw err;
        console.log("Error spotted, check log file for more information");
    });
}
