var io = require('socket.io');
var sanitizer = require('sanitizer');

var initPacket = {player:[],bullet:[]};
var removePacket = {player:[],bullet:[]};

Entity = param => {
    var self = {
        x:500,
        y:500,
        spdX:0,
        spdY:0,
        id:"",
        map:"forest"
    }
    if (param) {
        if (param.x) self.x = param.x;
        if (param.y) self.y = param.y;
        if (param.map) self.map = param.map;
        if (param.id) self.id = param.id;
    }
    self.update = () => {
        self.updatePosition();
    }
    self.updatePosition = () => {
        self.x += self.spdX;
        self.y += self.spdY;
    }
    self.getDistance = point => { //Call it point cuz it has x and y, makes sense? I guess
        return Math.sqrt(Math.pow(self.x-point.x,2) + Math.pow(self.y-point.y,2));
    }
    return self;
};

Entity.getFrameUpdateData = () => {
    var packet = {
        initPacket:{
            player:initPacket.player,
            bullet:initPacket.bullet
        },
        removePacket:{
            player:removePacket.player,
            bullet:removePacket.bullet
        },
        updatePacket:{
            player:Player.update(),
            bullet:Bullet.update()
        }
    };
    initPacket.player = [];
    initPacket.bullet = [];
    removePacket.player = [];
    removePacket.bullet = [];
    return packet;
}

Player = param => {
    var self = Entity(param);
    self.name = param.name;
    self.movingRight = false;
    self.movingLeft = false;
    self.movingUp = false;
    self.movingDown = false;
    self.leftClick = false;
    self.mouseAngle = 0;
    self.speed = 10;
    self.hp = 10;
    self.maxHp = 10;
    self.exp = param.progress.exp || 0;
    self.inventory = new Inventory(param.progress.items,param.socket,true);

    var superUpdate = self.update;
    self.update = () => {
        self.updateSpeed();
        superUpdate();

        if(self.leftClick){
            self.shootBullet(self.mouseAngle);
        }
    }
    self.shootBullet = angle => {
        if (Math.random() < 0.1) self.inventory.addItem("potion", 1);
        Bullet({
            parent:self.id,
            angle:angle,
            x:self.x,
            y:self.y,
            map:self.map
        });
    }

    self.updateSpeed = () => {
        if(self.movingRight) self.spdX = self.speed;
        else if(self.movingLeft) self.spdX = -self.speed;
        else self.spdX = 0;

        if(self.movingUp) self.spdY = -self.speed;
        else if(self.movingDown) self.spdY = self.speed;
        else self.spdY = 0;
    }

    self.getInitPacket = () => {
        return {
            id:self.id,
            x:self.x,
            y:self.y,
            name:self.name,
            hp:self.hp,
            maxHp:self.maxHp,
            exp:self.exp,
            map:self.map
        }
    }

    self.getUpdatePacket = () => {
        return {
            id: self.id,
            x:self.x,
            y:self.y,
            hp:self.hp,
            exp:self.exp,
            map:self.map
        }
    }

    Player.list[self.id] = self;

    initPacket.player.push( self.getInitPacket());
    return self;
};
Player.list = {};

Player.onConnect = function(socket,name,progress) {
    console.log("Player connected");
    var map = 'forest';
    if(Math.random() < 0.5) map = 'field';
    var player = Player({
        name:name,
        id:socket.id,
        map:map,
        socket:socket,
        progress:progress
    });
    player.inventory.refreshRender();
    socket.on('keyPress', data => {
        if(data.inputId === 'up') player.movingUp = data.state;
        else if(data.inputId === 'left') player.movingLeft = data.state;
        else if(data.inputId === 'down') player.movingDown = data.state;
        else if(data.inputId === 'right') player.movingRight = data.state;
        else if(data.inputId === 'attack') player.leftClick = data.state;
        else if(data.inputId === 'mouseAngle') player.mouseAngle = data.state;
    });

    socket.on('changeMap', data => {
        if (player.map === 'forest') player.map = 'field';
        else player.map = 'forest';
    })

    // Handle messages
    socket.on('sendMsg', msg => {
        let d = new Date();
        let msgSeconds = d.getSeconds();
        let msgMinutes = d.getMinutes();
        let msgHours = d.getHours();
        if (msgSeconds < 10) msgSeconds = '0' + msgSeconds;
        if (msgMinutes < 10) msgMinutes = '0' + msgMinutes;
        if (msgHours < 10) msgHours = '0' + msgHours;
        let displayDate = '[' + msgHours + ':' + msgMinutes + ':' + msgSeconds + '] ';
        // Send message to all active sockets, but first sanitize the text to prevent code injection
        for (var i in SOCKET_LIST) {
            var socket = SOCKET_LIST[i];
            socket.emit('writeOnChat', {
                msg: displayDate + player.name + ': ' + sanitizer.escape(msg),
                color: 'yellow'
            });
        }
    });

    socket.on('sendPrivateMsg', msg => { // msg:{username,message}
        var toPlayer = null;
        let d = new Date();
        let msgSeconds = d.getSeconds();
        let msgMinutes = d.getMinutes();
        let msgHours = d.getHours();
        if (msgSeconds < 10) msgSeconds = '0' + msgSeconds;
        if (msgMinutes < 10) msgMinutes = '0' + msgMinutes;
        if (msgHours < 10) msgHours = '0' + msgHours;
        let displayDate = '[' + msgHours + ':' + msgMinutes + ':' + msgSeconds + '] ';
        for (var i in Player.list) {
            if (Player.list[i].name === msg.name) toPlayer = SOCKET_LIST[i];
        }
        if (toPlayer === null) {
            socket.emit('writeOnChat',{
            msg: 'Player ' + msg.name + ' is not online.',
            color: 'white'
            });
         }
        else {
            toPlayer.emit('writeOnChat', {
                msg: displayDate + 'From ' + player.name + ": " + sanitizer.escape(msg.message),
                color: '#FF1493'
            });
            socket.emit('writeOnChat', {
                msg: displayDate + 'To ' + msg.name + ": " + sanitizer.escape(msg.message),
                color: '#FF1493'
            });
        }

    });

    socket.emit('initialize',{
        selfId:socket.id,
        player: Player.getAllInitPacket(),
        bullet: Bullet.getAllInitPacket()
    });
    
}

Player.getAllInitPacket = () => {
    var players = [];
    for (var i in Player.list) players.push(Player.list[i].getInitPacket());
    return players;
}

Player.onDisconnect = socket => {
    var player = Player.list[socket.id];
    if(!player) return;
    Database.savePlayerProgress({
        username:player.name,
        items:player.inventory.items,
        exp:player.exp
    });
    delete Player.list[socket.id];
    removePacket.player.push(socket.id);
    console.log("Player disconnected");
}

Player.update = () => {
    var packet = [];
    for(var i in Player.list) {
        //For each player in PLAYER_LIST we move x and y one time every clock tick and push it into the packet
        var player = Player.list[i];
        player.update();
        packet.push(player.getUpdatePacket());
    }
    return packet;
}

Bullet = function(param) {
    var self = Entity(param);
    self.id = Math.random();
    self.angle = param.angle;
    self.spdX = Math.cos(param.angle/180*Math.PI) * 10;
    self.spdY = Math.sin(param.angle/180*Math.PI) * 10;
    self.parent = param.parent;
    self.timer = 0;
    self.toRemove = false;
    var superUpdate = self.update;
    self.update = () => {
        if(self.timer++ > 100) self.toRemove = true;
        superUpdate();

        for(var i in Player.list) {
            var player = Player.list[i];
            if(self.map === player.map && self.getDistance(player) < 32 && self.parent !== player.id) {
                //handle collision. eg: hp--;
                player.hp -= 1;
                if (player.hp <= 0) {
                    var shooter = Player.list[self.parent];
                    if (shooter) shooter.exp += 1;
                    player.hp = player.maxHp;
                    player.x = Math.random() * 520;
                    player.y = Math.random() * 520;
                }
                self.toRemove=true;
            }
        }
    }

    self.getInitPacket = () => {
        return {
            id:self.id,
            x:self.x,
            y:self.y,
            map:self.map
        }
    }

    self.getUpdatePacket = () => {
        return {
            id: self.id,
            x:self.x,
            y:self.y
        }
    }

    Bullet.list[self.id] = self;
    initPacket.bullet.push(self.getInitPacket());
    return self;
}
Bullet.list = {};

Bullet.update = () => {
    var packet = [];
    for(var i in Bullet.list) {
        //For each player in PLAYER_LIST we move x and y one time every clock tick and push it into the packet
        var bullet = Bullet.list[i];
        bullet.update();
        if (bullet.toRemove) {
            delete Bullet.list[i]; //Check if bullet needs to be removed already and if so, delete it!
            removePacket.bullet.push(bullet.id);
        } else {
            packet.push(bullet.getUpdatePacket());
        }
    }
    return packet;
}

Bullet.getAllInitPacket = () => {
    var bullets = [];
    for (var i in Bullet.list) bullets.push(Bullet.list[i].getInitPacket());
    return bullets;
}