// DATABASE
var mongojs = require("mongojs");
db = mongojs(config.database.ip+":"+config.database.port+"/"+config.database.db,['account','progress']);

Database = {};
Database.isValidPassword = function(data,cb) {
    db.account.findOne({username:data.username,password:data.password},function(err,res) {
        if(res) cb(true);
        else cb(false);
    });
}

Database.isUsernameTaken = function(data,cb) {
    db.account.findOne({username:data.username},function(err,res) {
        if(res) cb(true);
        else cb(false);
    });
}

Database.addUser = function(data,cb){
	db.account.insert({username:data.username,password:data.password},function(err){
        Database.savePlayerProgress({username:data.username,items:[],exp:0},function(){
            cb();
        })
	});
}
Database.getPlayerProgress = function(username,cb){
	db.progress.findOne({username:username},function(err,res){
		cb({items:res.items});
	});
}
Database.savePlayerProgress = function(data,cb){
    cb = cb || function(){}
    db.progress.update({username:data.username},data,{upsert:true},cb);
}